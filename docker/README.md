Success! Created assets at /var/www/bf_apelsin/resources/assets
Inside that directory, you can run several commands:

```yarn start```
    Starts the development server.
```docker-compose run -p 3000:3000 node yarn start```

```yarn build```
    Bundles the app into static files for production.
```docker-compose run -p 3000:3000 node yarn build```

```yarn test```
    Starts the test runner.
```docker-compose run -p 3000:3000 node yarn test```

```yarn eject```
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!
```docker-compose run -p 3000:3000 node yarn eject```

We suggest that you begin by typing:

  ```docker-compose run -p 3000:3000 node yarn start```

Happy hacking!
